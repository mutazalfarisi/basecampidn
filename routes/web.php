<?php

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\ExportController;
use App\Http\Controllers\admin\KelolaController;
use App\Http\Controllers\admin\ManajemenUserController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\front\FrontController;
use App\Http\Controllers\Auth\AuthAdmin;
use App\Http\Controllers\front\DetailprodukController;
use App\Http\Controllers\front\ProdukController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use Illuminate\Support\Facades\Route;

use function Pest\Laravel\get;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// CUSTOMER

Route::get('/', [FrontController::class, 'index'])->name('home');
Route::get('/search-produk', [FrontController::class, 'search_produk']);
Route::get('/search/lihat_semua/{keyword}', [FrontController::class, 'result_produk']);

// ADMIN
Route::get('/detail_kaos/{id}', [DetailprodukController::class, 'detail_kaos']);
Route::get('/detail_tas/{id}', [DetailprodukController::class, 'detail_tas']);

Route::get('loginadmin', [AuthAdmin::class, 'create'])->name('loginadmin');
Route::post('loginadmin', [AuthAdmin::class, 'store']);

Route::get('get_kabupaten', [UserController::class, 'get_kabupaten'])->name('get_kabupaten');
Route::get('get_kecamatan', [UserController::class, 'get_kecamatan'])->name('get_kecamatan');

Route::get('/produk_kaos', [ProdukController::class, 'produk_kaos']);
Route::get('/produk_tas', [ProdukController::class, 'produk_tas']);

Route::middleware(['auth', 'checkrole:customer'])->group(function () {
    Route::get('/profil', [UserController::class, 'profil'])->name('profil_user');
    Route::post('/simpanprofil', [UserController::class, 'simpan'])->name('simpan_profil');
    Route::get('/jml_keranjang', [CartController::class, 'jml_keranjang'])->name('jml_keranjang');
    Route::post('/simpan_keranjang', [CartController::class, 'simpan'])->name('tambah_keranjang');
    Route::post('/hapus_keranjang', [CartController::class, 'hapus'])->name('hapus_keranjang');
    Route::post('/checkout_keranjang', [CartController::class, 'checkout'])->name('checkout_keranjang');
    Route::get('/keranjang', [CartController::class, 'index'])->name('keranjang');
    Route::get('/checkout/{id}', [CheckoutController::class, 'index'])->name('checkout');
    Route::get('/cek_ongkir', [CheckoutController::class, 'cek_ongkir'])->name('cek_ongkir');
    Route::post('/buat_pembayaran', [CheckoutController::class, 'buat_pembayaran'])->name('buat_pembayaran');
    Route::post('/terima_pesanan', [CheckoutController::class, 'terima_pesanan'])->name('terima_pesanan');
    Route::post('/upload_bukti', [CheckoutController::class, 'upload_bukti'])->name('upload_bukti');
    Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])
        ->name('logout');
});

Route::middleware(['auth', 'checkrole:admin'])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard_admin');
        Route::get('/profil', [AdminController::class, 'profil'])->name('profil_admin');
        Route::get('/alamat_toko', [AdminController::class, 'alamat_toko'])->name('alamat_toko');
        Route::post('/simpan_profil', [AdminController::class, 'simpan_profil'])->name('simpan_profil_admin');
        Route::post('/simpan_toko', [AdminController::class, 'simpan_toko'])->name('simpan_toko');

        Route::get('/kelola/kaos', [KelolaController::class, 'kelola_kaos'])->name('kelola_kaos');
        Route::get('/kelola/tas', [KelolaController::class, 'kelola_tas'])->name('kelola_tas');

        Route::get('/daftar_admin', [ManajemenUserController::class, 'daftar_admin'])->name('daftar_admin');
        Route::get('/daftar_customer', [ManajemenUserController::class, 'daftar_customer'])->name('daftar_customer');
        Route::post('/tambah_admin', [ManajemenUserController::class, 'tambah_admin'])->name('tambah_admin');
        Route::post('/hapus_admin', [ManajemenUserController::class, 'hapus'])->name('hapus_admin');

        Route::get('/transaksi/kaos', [KelolaController::class, 'transaksi_kaos'])->name('transaksi_kaos');
        Route::get('/transaksi/tas', [KelolaController::class, 'transaksi_tas'])->name('transaksi_tas');

        Route::get('/pemasukan', [KelolaController::class, 'pemasukan'])->name('pemasukan');
        Route::get('/pemasukan_filter', [KelolaController::class, 'pemasukan_filter'])->name('pemasukan_filter');
        Route::get('/get_data_pemasukan', [KelolaController::class, 'get_data_pemasukan'])->name('get_data_pemasukan');


        Route::get('/tambah/produk', [KelolaController::class, 'tambah_produk']);
        Route::get('/edit/produk/{id}', [KelolaController::class, 'edit_produk']);
        Route::post('/simpan/produk', [KelolaController::class, 'simpan']);

        Route::get('/tambah/kaos', [KelolaController::class, 'tambah_kaos']);
        Route::get('/tambah/tas', [KelolaController::class, 'tambah_tas']);

        Route::post('/validasi', [KelolaController::class, 'validasi_transaksi'])->name('validasi_transaksi');
        Route::post('/ganti_status', [KelolaController::class, 'ganti_status'])->name('ganti_status');
        Route::get('/detail_transaksi', [KelolaController::class, 'get_detail_transaksi'])->name('detail_transaksi');

        Route::get('/cetak/transaksi_bulan', [ExportController::class, 'cetak_transaksi_bulan'])->name('cetak_transaksi_bulan');
        Route::get('/cetak/cetak_transaksi', [ExportController::class, 'cetak_transaksi'])->name('cetak_transaksi');

        Route::post('/logout_admin', [AuthAdmin::class, 'destroy'])->name('logout_admin');
    });
});

Route::get('/cetak/transaksi/{id}', [ExportController::class, 'cetak'])->name('cetak_invoice');

require __DIR__ . '/auth.php';
