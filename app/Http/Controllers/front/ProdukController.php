<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\t_produk;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProdukController extends Controller
{
    function produk_kaos()
    {
        $dt_kaos = t_produk::whereNull('deleted_at')->where('id_kategori', 1)->get();
        $data = [
            'li_active' => "tshirt",
            'dt_kaos' => $dt_kaos,
        ];
        return view('front/produk/produk_kaos', $data);
    }

    function produk_tas()
    {
        $dt_tas = t_produk::whereNull('deleted_at')->where('id_kategori', 2)->get();
        $data = [
            'li_active' => "bag",
            'dt_tas' => $dt_tas
        ];
        return view('front/produk/produk_tas', $data);
    }
}
