<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\detail_transaksi;
use App\Models\Kategori;
use App\Models\t_produk;
use App\Models\t_transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use PDF;

class ExportController extends Controller
{
    public function cetak($id)
    {
        $store = DB::table('store')->first();
        $data = DB::table('t_transaksi as a')
            ->select('a.*', 'b.id_produk', 'd.harga', 'd.stok', 'd.foto', 'd.nama_produk', 'd.deskripsi', 'b.jml_barang', 'c.ukuran')
            ->leftJoin('detail_transaksi as b', 'a.id', '=', 'b.id_transaksi')
            ->leftJoin('ref_ukuran as c', 'b.id_ukuran', '=', 'c.id')
            ->leftJoin('t_produk as d', 'b.id_produk', '=', 'd.id')
            ->where('a.id', $id)
            ->get();
        $dt = [
            'data' => $data,
            'store' => $store,
        ];

        $pdf = PDF::loadView('admin.cetak.invoice', $dt);
        return $pdf->stream('invoice.pdf');
    }

    public function cetak_transaksi(Request $request)
    {
        $store = DB::table('store')->first();
        $tglMulai = $request->input('tgl_mulai');
        $tglSelesai = $request->input('tgl_selesai');
        $query = DB::table('t_transaksi as a')
            ->select('a.*', 'b.id_produk', 'd.harga', 'd.stok', 'd.foto', 'd.nama_produk', 'd.deskripsi', 'b.jml_barang', 'c.ukuran', 'f.name')
            ->leftJoin('detail_transaksi as b', 'a.id', '=', 'b.id_transaksi')
            ->leftJoin('ref_ukuran as c', 'b.id_ukuran', '=', 'c.id')
            ->leftJoin('t_produk as d', 'b.id_produk', '=', 'd.id')
            ->leftJoin('users as f', 'a.id_user', '=', 'f.id');
        if ($tglMulai != null || $tglSelesai != null) {
            $query->whereBetween('a.tgl_transaksi', [$tglMulai, $tglSelesai]);
        }
        $data = $query->get();
        $dt = [
            'data' => $data,
            'store' => $store,
        ];

        $pdf = PDF::loadView('admin.cetak.rekap_transaksi', $dt);
        return $pdf->stream('rekap_transaksi.pdf');
    }

    public function cetak_transaksi_bulan($bulan)
    {
        $store = DB::table('store')->first();
        // $data = t_transaksi::select('a.*', 'b.name')->from('t_transaksi as a')->leftjoin('users as b', 'a.id_user', '=', 'b.id')->where('a.status_barang', '3')->whereMonth('a.tgl_terima', '=', now()->format('m'))->where('a.tgl_terima', '!=', 'null')->get();
        $data = t_transaksi::select('a.*', 'b.name')->from('t_transaksi as a')->leftjoin('users as b', 'a.id_user', '=', 'b.id')->where('a.status_barang', '3')->whereMonth('a.tgl_terima', '=', $bulan)->where('a.tgl_terima', '!=', 'null')->get();
        $dt = [
            'data' => $data,
            'store' => $store,
            'bulan' => $bulan
        ];

        $pdf = PDF::loadView('admin.cetak.transaksi_perbulan', $dt);
        return $pdf->stream('rekap_transaksi_' . getBulan() . '.pdf');
    }
}
