<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;
use Illuminate\View\View;

class NewPasswordController extends Controller
{
    /**
     * Display the password reset view.
     */
    public function create(Request $request)
    {   
        $data = [
            'token'=>$request->route('token'),
            'email'=>$request->query('email')
        ];
        return view('auth.reset-password', $data);
    }

    /**
     * Handle an incoming new password request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'token' => ['required'],
            'email' => ['required', 'email'],
            // 'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'password' => ['required'],
            'confirm_password' => 'required|same:password',
        ],[
            'token.required'=> 'Token anda tidak valid.',
            'email.required'=> 'Email anda kosong.',
            'email.email' => 'Format email anda salah.',
            'password.required'=> 'Password anda kosong.',
            'confirm_password.required' => 'Password anda kosong.',
            'confirm_password.same' => 'Password anda tidak cocok.'
        ]);

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.

        $cek_user = User::where('email',$request->email)->first();
        if ($cek_user != null) {
            $dt = [
                'password'=> Hash::make($request->password),
                'reset_id' => $request->password,
                'updated_at'=> now()->format('Y-m-d H:i:s'),
                'remember_token' => $request->token
            ];
            $update = User::where('email',$request->email)->update($dt);
            if ($update) {
                session()->flash('success', 'Berhasil Memperbarui Password.');
            }else{
                session()->flash('error', 'Gagal Memperbarui Password.');
            }
        }else{
            session()->flash('error', 'Akun anda tidak ditemukan.');
        }

        return redirect()->route('login');
        // $status = Password::reset(
        //     $request->only('email', 'password', 'password_confirmation', 'token'),
        //     function ($user) use ($request) {
        //         $user->forceFill([
        //             'password' => Hash::make($request->password),
        //             'remember_token' => Str::random(60),
        //         ])->save();

        //         event(new PasswordReset($user));
        //     }
        // );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        // return $status == Password::PASSWORD_RESET
        //             ? redirect()->route('login')->with('status', __($status))
        //             : back()->withInput($request->only('email'))
        //                     ->withErrors(['email' => __($status)]);
    }
}
