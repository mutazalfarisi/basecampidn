<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\LoginRequestAdmin;
use App\Providers\RouteServiceProvider;
use App\Providers\RouteServiceProviderAdmin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use Illuminate\Support\Facades\Session;

class AuthAdmin extends Controller
{
    /**
     * Display the login view.
     */
    public function create()
    {
        if (isset(auth()->user()->username)) {
            return redirect()->route('dashboard_admin');
        }
        $data = [
            "js" => @include('admin.login_js')
        ];
        return view('admin.login', $data);
    }

    /**
     * Handle an incoming authentication request.
     */

    public function store(Request $request)
    {
        $input = $request->all();
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        if (auth()->attempt(array('username' => $input['username'], 'password' => $input['password']))) {
            if (auth()->user()->role == 'admin') {
                return redirect()->route('dashboard_admin');
            } else {
                return redirect()->route('home');
            }
        } else {
            return redirect()->route('login')
                ->with('error', 'Email-Address And Password Are Wrong.');
        }
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request)
    {
        Session::flush();

        Auth::logout();

        return redirect('/login');
        // return redirect('/loginadmin');
        // Auth::guard('admins')->logout();

        // $request->session()->invalidate();

        // $request->session()->regenerateToken();

    }
}
