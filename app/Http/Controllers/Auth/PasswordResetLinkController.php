<?php

namespace App\Http\Controllers\Auth;

// use App\Http\Controllers\Controller;
// use Illuminate\Http\RedirectResponse;
// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Password;
use Illuminate\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; 
use App\Models\User; 
use App\Mail\ResetPasswordMail;
use Illuminate\Support\Facades\Mail;
// use Mail; 
use Hash;
use Illuminate\Support\Str;

class PasswordResetLinkController extends Controller
{
    /**
     * Display the password reset link request view.
     */
    public function create(): View
    {
        return view('auth.forgot-password');
    }

    /**
     * Handle an incoming password reset link request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
        ]);
        
        $resetLink = [
            'token' => $request->_token,
            'email' => $request->email
        ];

        $send = Mail::to($request->email)->send(new ResetPasswordMail($resetLink));
        if ($send) {
            session()->flash('success', 'Link reset password sudah dikirimkan ke '.$request->email.'. Silahkan di cek.');
        } else {
            session()->flash('error', 'Gagal Mengirimkan Reset Password');
        }

        return redirect()->route('login');
    }
}
