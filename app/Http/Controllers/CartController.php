<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Alamatuser;
use App\Models\cart;
use App\Models\detail_transaksi;
use App\Models\t_kab;
use App\Models\t_kec;
use App\Models\t_provinsi;
use App\Models\t_transaksi;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{

    function index()
    {
        $dt_keranjang = Cart::select('a.*', 'b.nama_produk', 'b.deskripsi', 'b.harga', 'b.foto', 'b.stok', 'c.ukuran')->from('cart as a')
            ->leftJoin('t_produk as b', 'a.id_produk', '=', 'b.id')->leftJoin('ref_ukuran as c', 'a.ukuran', '=', 'c.id')
            ->where('a.id_user', auth()->user()->id)->whereNull('a.deleted_at')->where('a.status', '0')->orderby('a.id', 'desc')->get();


        $dt_belum_bayar = DB::table('t_transaksi as a')
            ->select('a.id', 'a.no_transaksi', 'a.tgl_transaksi', 'a.bukti_transfer', 'a.tgl_exp', 'c.nama_produk', 'c.foto', 'c.harga', 'c.deskripsi', 'b.jml_barang AS jumlah', 'd.ukuran', 'a.status_barang', 'a.status_pembayaran', 'e.nama_bank', 'e.no_rek AS rekening')
            ->leftJoin('detail_transaksi as b', 'a.id', '=', 'b.id_transaksi')
            ->join('t_produk as c', 'b.id_produk', '=', 'c.id')
            ->leftJoin('ref_ukuran as d', 'b.id_ukuran', '=', 'd.id')
            ->leftJoin('ref_bank as e', 'a.id_bank', '=', 'e.id')
            ->whereNull('a.deleted_at')
            ->where('a.tgl_exp', '>', now()->format('Y-m-d H:i:s'))
            ->where('a.status_barang', '=', '0')->orderby('a.id', 'desc')->get();

        $dt_exp = DB::table('t_transaksi as a')
            ->select('a.*', 'b.id_produk', 'd.harga', 'd.foto', 'd.nama_produk', 'd.deskripsi', 'b.jml_barang')
            ->leftJoin('detail_transaksi as b', 'a.id', '=', 'b.id_transaksi')
            ->leftJoin('ref_ukuran as c', 'b.id_ukuran', '=', 'c.id')
            ->leftJoin('t_produk as d', 'b.id_produk', '=', 'd.id')
            ->whereNull('a.deleted_at')
            ->where('a.tgl_exp', '<', now()->format('Y-m-d H:i:s'))
            ->where('a.status_pembayaran', '0')
            ->get();

        $dt_proses = DB::table('t_transaksi as a')
            ->select('a.*', 'b.id_produk', 'd.harga', 'd.foto', 'd.nama_produk', 'd.deskripsi', 'b.jml_barang')
            ->leftJoin('detail_transaksi as b', 'a.id', '=', 'b.id_transaksi')
            ->leftJoin('ref_ukuran as c', 'b.id_ukuran', '=', 'c.id')
            ->leftJoin('t_produk as d', 'b.id_produk', '=', 'd.id')
            ->whereNull('a.deleted_at')
            ->whereIn('a.status_barang', ['0', '1', '2'])
            ->where('a.status_pembayaran', '=', '1')
            ->get();

        $dt_transaksi = DB::table('t_transaksi as a')
            ->select('a.*', 'b.id_produk', 'd.harga', 'd.foto', 'd.nama_produk', 'd.deskripsi', 'b.jml_barang')
            ->leftJoin('detail_transaksi as b', 'a.id', '=', 'b.id_transaksi')
            ->leftJoin('ref_ukuran as c', 'b.id_ukuran', '=', 'c.id')
            ->leftJoin('t_produk as d', 'b.id_produk', '=', 'd.id')
            ->whereNull('a.deleted_at')
            ->whereIn('a.status_barang', ['3'])
            ->where('a.status_pembayaran', '=', '1')
            ->get();
        // print_r($dt_transaksi);die;

        $data = [
            'title' => 'Keranjang | Basecampidn.',
            'li_active' => "keranjang",
            'dt_keranjang' => $dt_keranjang,
            'dt_belum_bayar' => $dt_belum_bayar,
            'dt_exp' => $dt_exp,
            'dt_proses' => $dt_proses,
            'dt_riwayat' => $dt_transaksi,
            'script_js' => 'customer/keranjang.js'
        ];
        return view('front/keranjang', $data);
    }

    function jml_keranjang()
    {
        $jml = cart::where('id_user', Auth::user()->id)->where('status', '0')->whereNull('deleted_at')->count();
        echo json_encode($jml);
    }

    function simpan(Request $request)
    {
        $dt = [
            'id_user' => Auth::user()->id,
            'id_produk' => $request->id_produk,
            'jumlah' => $request->jumlah,
            'ukuran' => $request->ukuran,
            'status' => '0',
            'created_at' => now()->format('Y-m-d H:i:s')
        ];
        $insert = cart::insert($dt);
        if ($insert) {
            $res = [
                'status' => true,
                'pesan' => "Berhasil Menambahkan Ke Keranjang."
            ];
        } else {
            $res = [
                'status' => false,
                'pesan' => "Gagal Menambahkan Ke Keranjang."
            ];
        }

        echo json_encode($res);
    }

    function hapus(Request $request)
    {
        $id = $request->id;
        $dt = ['deleted_at' => now()->format('Y-m-d H:i:s')];
        $insert = cart::where('id', $id)->update($dt);
        if ($insert) {
            $res = [
                'status' => true,
                'pesan' => "Berhasil Menghapus Ke Keranjang."
            ];
        } else {
            $res = [
                'status' => false,
                'pesan' => "Gagal Menghapus Ke Keranjang."
            ];
        }

        echo json_encode($res);
    }

    function checkout(Request $request)
    {
        $id = $request->id;
        // $dt_cart = [
        //     'status' => '1'
        // ];
        // cart::where('id',$id)->update($dt_cart);
        // $get_data_produk = cart::where('id', $id)->first();
        // $dt_transaksi = [
        //     'id_user' => auth()->user()->id,
        //     'no_transaksi' => uniqid(),
        //     'tgl_transaksi' => now()->format('Y-m-d H:i:s'),
        //     'tgl_exp' => now()->addHours(24)->format('Y-m-d H:i:s'),
        //     'status_barang' => '0',
        //     'status_pembayaran' => '0',
        //     'created_at' => now()->format('Y-m-d H:i:s'),
        // ];
        // $id_transaksi = t_transaksi::insertGetId($dt_transaksi);
        // $dt_detail = [
        //     'id_transaksi' => $id_transaksi,
        //     'id_produk' => $get_data_produk->id_produk,
        //     'id_ukuran' => $get_data_produk->ukuran,
        //     'jml_barang' => $get_data_produk->jumlah,
        //     'created_at' => now()->format('Y-m-d H:i:s')
        // ];
        // $insert = detail_transaksi::insert($dt_detail);
        // if ($insert) {
        //     $res = [
        //         'status' => true,
        //         'pesan' => "Berhasil CheckOut."
        //     ];
        // }else{
        //     $res = [
        //         'status' => false,
        //         'pesan' => "Gagal CheckOut."
        //     ];
        // }

        // echo json_encode($res);
        echo json_encode($id);
    }
}
