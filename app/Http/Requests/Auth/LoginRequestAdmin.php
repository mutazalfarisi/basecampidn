<?php

namespace App\Http\Requests\Auth;

use App\Providers\RouteServiceProviderAdmin;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

use function PHPUnit\Framework\at;

class LoginRequestAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'username' => ['required', 'string'],
            'password' => ['required', 'string'],
        ];
    }

    /**
     * Attempt to authenticate the request's credentials.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function authenticate(): void
    {
        $this->ensureIsNotRateLimited();

        if (! Auth::attempt($this->only('username', 'password'), $this->boolean('remember'))) {
            RateLimiter::hit($this->throttleKey());

            throw ValidationException::withMessages([
                'username' => trans('auth.failed'),
            ]);
        }

        RateLimiter::clear($this->throttleKey());
    }
    // public function authenticate(): void
    // {
    //     $this->ensureIsNotRateLimited();

    //     // Melakukan otentikasi dengan username dan password yang diberikan
    //     if (Auth::attempt($this->only('username', 'password'), $this->boolean('remember'))) {
    //         // Memeriksa apakah pengguna memiliki peran 'admin'
    //         if (Auth::user()->hasRole('admin')) {
    //             RateLimiter::clear($this->throttleKey());
    //             return redirect()->intended(RouteServiceProviderAdmin::HOME); // Redirect ke halaman yang sesuai setelah login berhasil
    //         } else {
    //             Auth::logout(); // Logout pengguna jika tidak memiliki peran 'admin'
    //         }
    //     } else {
    //         RateLimiter::hit($this->throttleKey());

    //         throw ValidationException::withMessages([
    //             'username' => trans('auth.failed'),
    //         ]);
    //     }
    // }

    // Metode hasRole() pada model pengguna (User.php)
    // public function hasRole($role)
    // {
    //     return $this->role === $role;
    // }

    /**
     * Ensure the login request is not rate limited.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function ensureIsNotRateLimited(): void
    {
        if (!RateLimiter::tooManyAttempts($this->throttleKey(), 5)) {
            return;
        }

        event(new Lockout($this));

        $seconds = RateLimiter::availableIn($this->throttleKey());

        throw ValidationException::withMessages([
            'username' => trans('auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ]),
        ]);
    }

    /**
     * Get the rate limiting throttle key for the request.
     */
    public function throttleKey(): string
    {
        return Str::transliterate(Str::lower($this->input('username')) . '|' . $this->ip());
    }
}
