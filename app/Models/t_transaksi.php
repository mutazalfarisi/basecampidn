<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class t_transaksi extends Model
{
    protected $table = 't_transaksi';
    protected $guarded = [];
}
