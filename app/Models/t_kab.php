<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class t_kab extends Model
{
    protected $table = 'ref_kab';
    protected $guarded = [];
}
