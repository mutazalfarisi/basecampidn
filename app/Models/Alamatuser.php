<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alamatuser extends Model
{
    protected $table = 'alamatuser';
    protected $guarded = [];
}
