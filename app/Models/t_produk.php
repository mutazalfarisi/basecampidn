<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class t_produk extends Model
{
    protected $table = 't_produk';
    protected $guarded = [];
}
