<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alamatuser', function (Blueprint $table) {
            $table->id();
            $table->integer('id_user')->nullable();
            $table->string('province_id')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('city_id')->nullable();
            $table->string('kabupaten')->nullable();
            $table->string('subdistrict_id')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('detail_alamat')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alamatusers');
    }
};
