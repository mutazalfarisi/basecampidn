<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_transaksi', function (Blueprint $table) {
            $table->id();
            $table->integer('id_user')->nullable();
            $table->integer('is_admin')->nullable();
            $table->string('no_transaksi')->nullable();
            $table->dateTime('tgl_transaksi')->nullable();
            $table->dateTime('tgl_exp')->nullable();
            $table->dateTime('tgl_terima')->nullable();
            $table->string('kurir')->nullable();
            $table->string('jenis_pengiriman')->nullable();
            $table->integer('harga_ongkir')->nullable();
            $table->integer('harga_item')->nullable();
            $table->integer('harga_total')->nullable();
            $table->string('bukti_transfer')->nullable();
            $table->enum('status_barang',['0','1','2','3'])->nullable()->defaul('0');
            // KETERANGAN
            // 0 = Dalam Persetujuan
            // 1 = Sedang Di Kemas
            // 2 = Di kirim
            // 3 = Sampai
            $table->enum('status_pembayaran',['0','1'])->nullable()->defaul('0');
            // KETERANGAN
            // 0 = Belum bayar
            // 1 = Sudah Bayar
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_transaksis');
    }
};
