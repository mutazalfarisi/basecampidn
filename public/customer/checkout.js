$(document).ready(function(){
    $("#kurir").select2();
    $("#jenis_pengiriman").select2();
})

function simpan(){
    let id_keranjang = $("#id_keranjang").val();
    let harga_item = $("#harga_item").val();
    let harga_total = $("#harga_total").val();
    let harga_ongkir = $("#harga_ongkir").val();
    let id_bank = $("#id_bank").val();
    let kurir = $("#kurir").val();
    let jenis_pengiriman = $("#jenis_pengiriman").val();
    let csrfToken = $('meta[name="csrf-token"]').attr('content');
    Swal.fire({
        text: "Anda yakin ingin menambahkan ke pembayaran?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya, Tambahkan'
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/buat_pembayaran',
                type:'post',
                data:{
                    id:id_keranjang,
                    harga_total:harga_total,
                    harga_item:harga_item,
                    id_bank:id_bank,
                    harga_ongkir:harga_ongkir,
                    kurir:kurir,
                    jenis_pengiriman:jenis_pengiriman,
                    _token: csrfToken
                },
                dataType:'json',
                success:function(res){
                    if (res.status) {
                        Swal.fire(
                            'Berhasil!',
                            res.pesan,
                            'success'
                            )
                        load_cart();
                        setTimeout(function() {
                            window.location.href = res.url; // true mengabaikan cache dan memaksa memuat ulang dari server
                        }, 1000);
                    }else{
                        Swal.fire(
                            'Gagal!',
                            res.pesan,
                            'error'
                            )
                            setTimeout(function() {
                                location.reload(true);
                            }, 2000);
                    }
                }
            })
        }
      })
}

function cek_ongkir(dt){
    let kurir = $(dt).val();
    let berat = 500 * total_item ;
    let tujuan = alamat_tujuan;
    $("#jenis_pengiriman").html('<option value="">Loading..</option>')
    $("#estimasi").html('Pilih Jenis Pengiriman');
    $.ajax({
        url:'/cek_ongkir',
        type:'get',
        dataType:'json',
        data:{
            kurir:kurir,
            berat:berat,
            tujuan:tujuan
        },
        success:function(res){
            let option = '';
            option += '<option value="">Pilih Pengiriman</option>';
            $.each(res, function(index, item) {
                option += '<option data-harga="'+item.cost[0].value+'" data-estimasi="'+item.cost[0].etd+'" value="'+ item.service +'">'+item.service +' ( Rp ' +  item.cost[0].value + ')'+'</option>';
            });
            $("#jenis_pengiriman").html(option)
        }
    })
}

function jenis_pengiriman(dt){
    let kurir = $("#kurir").val();
    let jenis_pengiriman = $(dt).val()
    let harga_ongkir = $(dt).find(':selected').data('harga');
    let estimasi = $(dt).find(':selected').data('estimasi');
    let dt_ongkir = `<td>${kurir.toUpperCase()}</td><td>${jenis_pengiriman}</td><td style="text-align:right;">Rp. ${harga_ongkir}</td>`;
    $("#harga_ongkir").val(harga_ongkir)
    $("#dt_ongkir").show();
    $("#ongkir").html(dt_ongkir);
    $("#estimasi").html('Estimasi Sampai : '+ estimasi +'');
    let harga_akhir = parseInt(harga_total) + parseInt(harga_ongkir);
    $("#harga_total").val(harga_akhir)
    $("#total").html('Rp '+rupiah(harga_akhir))
}

function rupiah(angka) {
    var reverse = angka.toString().split('').reverse().join(''),
        ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join('.').split('').reverse().join('');
    return ribuan;
}

function hapus(dt){
    let id_cart = $(dt).data('id_cart');
    let csrfToken = $('meta[name="csrf-token"]').attr('content');
    Swal.fire({
        text: "Anda yakin ingin menghapus produk ini?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya, Hapus'
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/hapus_keranjang',
                type:'post',
                data:{
                    id:id_cart,
                    _token: csrfToken
                },
                dataType:'json',
                success:function(res){
                    if (res) {
                        Swal.fire(
                            'Berhasil!',
                            res.pesan,
                            'success'
                            )
                        load_cart();
                        setTimeout(function() {
                            location.reload(true); // true mengabaikan cache dan memaksa memuat ulang dari server
                        }, 1000);
                    }else{
                        Swal.fire(
                            'Gagal!',
                            res.pesan,
                            'error'
                            )
                    }
                }
            })
        }
      })
}
function pilih_bank(dt){
    // let no_rek = $(dt).data
    var selectedIndex = dt.selectedIndex;
    var selectedOption = dt.options[selectedIndex];

    var noRek = selectedOption.getAttribute('data-no_rek');


    console.log(noRek);
    if (noRek != null) {
        $("#no_rek").html(noRek);
    }else{
        $("#no_rek").html('Pilih Bank Terlebih Dahulu');
    }
}
function checkout(dt){
    let id_cart = $(dt).data('id_cart');
    let csrfToken = $('meta[name="csrf-token"]').attr('content');
    Swal.fire({
        text: "Anda yakin ingin CheckOut produk ini?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya, Check Out'
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/checkout_keranjang',
                type:'post',
                data:{
                    id:id_cart,
                    _token: csrfToken
                },
                dataType:'json',
                success:function(res){

                    // if (res) {
                    //     Swal.fire(
                    //         'Berhasil!',
                    //         res.pesan,
                    //         'success'
                    //         )
                    //     load_cart();
                    //     setTimeout(function() {
                    //         location.reload(true);
                    //     }, 1000);
                    // }else{
                    //     Swal.fire(
                    //         'Gagal!',
                    //         res.pesan,
                    //         'error'
                    //         )
                    // }
                }
            })
        }
      })
}
