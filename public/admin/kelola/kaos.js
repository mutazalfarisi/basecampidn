$(document).ready(function() {
    load_table();
})
function load_table(){
    $('#dataTable').DataTable({
      responsive: true
    });
}
function hapus(dt){
    let id = $(dt).data('id');
    Swal.fire({
        // title: 'Anda Yakin?',
        text: "Anda akan menghapus data ini, anda yakin?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya, Hapus'
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax()
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
        }
      })
}
