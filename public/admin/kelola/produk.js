$(document).ready(function() {
    // $('#dataTable').DataTable();
    $('#harga').on('input', function() {
        var unformatted = $(this).val().replace(/\./g, '').replace(/^0+/, ''); // Menghapus titik dan nol di awal angka 
        var rupiah = formatRupiah(unformatted);
        $(this).val(rupiah);
    });

    $('#stok').on('input', function() {
        $(this).val($(this).val().replace(/\D/g,''));
    });
})

function formatRupiah(angka) {
    var number_string = angka.toString().replace(/[^,\d]/g, ''),
        split = number_string.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return 'Rp ' + rupiah;
}
