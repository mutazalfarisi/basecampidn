$(document).ready(function() {
    load_table();
})

function load_table() {
    $('#dataTable').DataTable();
}

function show_detail(id){
    $.ajax({
        url:'/admin/detail_transaksi',
        type:'get',
        data:{
            id:id
        },dataType:'json',
        success:function(res){
            $("#modal-content").html(res)
        }
    })
    $("#modal_detail").modal('show');

}

function hapus(dt) {
    let id = $(dt).data('id');
    Swal.fire({
        text: "Anda akan menghapus data ini, anda yakin?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya, Hapus'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax()
            Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )
        }
    })
}

function ganti_status(v,id){
    let csrfToken = $('meta[name="csrf-token"]').attr('content');
    let status = '';
    if (v == 2) {
        status += 'Anda akan mengirim paket ini, anda yakin?';
    } else {
        status += 'Anda akan menyelesaikan paket ini, anda yakin?';
    }
    Swal.fire({
        text: status,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya, Setuju'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url:`/admin/ganti_status`,
                type:'post',
                dataType:'json',
                data:{
                    id:id,
                    value:v,
                    _token: csrfToken
                },
                success:function(res){
                    if (res.status) {
                        Swal.fire(
                            'Berhasil',
                            res.pesan,
                            'success'
                        )
                    }else{
                        Swal.fire(
                            'Gagal',
                            res.pesan,
                            'error'
                        )
                    }
                    setTimeout(function() {
                        location.reload(true); 
                    }, 1000);
                }
            });
        }
    })
}

function validasi(dt) {
    let id = $(dt).data('id');
    let tipe = $(dt).data('tipe');
    let csrfToken = $('meta[name="csrf-token"]').attr('content');
    Swal.fire({
        text: "Anda akan menyetujui data ini, anda yakin?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya, Setuju'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url:`/admin/validasi`,
                type:'post',
                dataType:'json',
                data:{
                    id:id,
                    tipe:tipe,
                    _token: csrfToken
                },
                success:function(res){
                    if (res.status) {
                        Swal.fire(
                            'Berhasil',
                            res.pesan,
                            'success'
                        )
                    }else{
                        Swal.fire(
                            'Gagal',
                            res.pesan,
                            'error'
                        )
                    }
                    setTimeout(function() {
                        location.reload(true); 
                    }, 1000);
                }
            });
        }
    })
}
