@extends('front.layout.default')

@section('content')
    <div class="content">
        <div class="hero__content d-flex justify-content-center align-items-center" style="min-height: 200px;" id="home">
            <div class="title-hero">
                <h1 class="text-center fw-bold" style="vertical-align: middle;">Produk Kaos.</h1>
            </div>
        </div>
        <div class="p-5 main-product">
            <div class="content__product" id="tshirt">
                <div class="p-4 title-hero">
                    <h3 class="text-center fw-bold" style="vertical-align: middle;">T-Shirt</h3>
                </div>
                <div class="flex-wrap gap-3 mt-3 img_content_product d-flex">
                    <div class="row justify-content-center">
                        @foreach ($dt_kaos as $item)
                            <div class="mt-2 col-6 col-md-3 me-2">
                                <div class="card" style="width: 18rem;">
                                    <img src="{{ asset('storage') . '/' . $item->foto }}" class="card-img-top"
                                        alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $item->nama_produk }}</h5>
                                        <p class="card-text">{{ $item->deskripsi }} <br>
                                            <span class="rupiah">Rp. {{ $item->harga }}</span>
                                        </p>
                                        <a href="{{ url('/detail_kaos/' . $item->id) }}" class="btn btn-primary">Lihat</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
