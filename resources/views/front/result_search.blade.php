@if (count($data) > 0)
    @foreach ($data as $item)
        <a href="{{ url('/detail_kaos/' . $item->id) }}">
            <div class="link-dropdown col-12">
                <div class="d-flex">
                    <div style="width:20%;">
                        <img src="{{ asset('storage') . '/' . $item->foto }}" style="width:100%;">
                    </div>
                    <div>
                        <span style="font-size: 12px;font-weight:bold;margin-left:20px;">{{ $item->nama_produk }}</span>
                    </div>
                </div>
            </div>
        </a>
    @endforeach
    <div class="text-center link-dropdown col-12">
        <a href="{{ url('/search/lihat_semua') . '/' . $keyword }}" style="font-size: 12px;font-weight:bold;">Lihat
            Semua</a>
    </div>
@else
    @if ($keyword == '' || $keyword == null)
        <div class="text-center link-dropdown col-12">
            <span style="font-size: 12px;font-weight:bold;">Apa yang sedang anda cari?</span>
        </div>
    @else
        <div class="text-center link-dropdown col-12">
            <div class="text-center fw-bold" style="font-size:12px;">Produk yang anda cari tidak ada</div>
        </div>
    @endif
@endif
