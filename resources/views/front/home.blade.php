@extends('front.layout.default')

@section('content')
    <div class="content">
        <div id="home">
            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner" style="height: 80vh;">
                    <div class="carousel-item active">
                        <img src="{{ asset('images/img9.jpg') }}" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('images/img2.jpg') }}" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('images/img4.jpg') }}" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('images/img3.jpg') }}" class="d-block w-100" alt="...">
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
            <div class="hero__content d-flex justify-content-center align-items-center" style="min-height: 90px;">
                <div class="title-hero">
                    <h1 class="text-center fw-bold" style="vertical-align: middle;">Basecampidn.</h1>
                </div>
            </div>
        </div>

        <div class="p-5 main-product">
            <div class="content__product" id="tshirt">
                <div class="p-4 title-hero">
                    <h3 class="text-center fw-bold" style="vertical-align: middle;">T-Shirt</h3>
                </div>
                <div class="gap-3 mt-3 img_content_product owl-carousel d-flex flex-nowrap">
                    @foreach ($dt_kaos as $item)
                        <div><a href="{{ url('/detail_kaos/' . $item->id) }}" style="text-decoration: none;color:black;">
                                <div class="card__product">
                                    <img src="{{ asset('storage') . '/' . $item->foto }}" class="img__product"
                                        style="" alt="">
                                    <div class="p-3 judul_product">
                                        <p>{{ $item->nama_produk }}<br> <span class="rupiah">Rp. {{ $item->harga }}</span>
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="p-4 text-center title-hero">
                    <a href="{{ url('/produk_kaos') }}"><button class="btn-cstm-blue">Show More</button></a>
                </div>
            </div>
            <hr>
            <div class="content__product" id="bag">
                <div class="p-4 title-hero">
                    <h3 class="text-center fw-bold" style="vertical-align: middle;">Bag</h3>
                </div>
                <div class="gap-3 mt-3 img_content_product owl-carousel d-flex flex-nowrap">
                    @foreach ($dt_tas as $item)
                        <div><a href="{{ url('/detail_tas/' . $item->id) }}" style="text-decoration: none;color:black;">
                                <div class="card__product">
                                    <img src="{{ asset('storage') . '/' . $item->foto }}" class="img__product"
                                        style="" alt="">
                                    <div class="p-3 judul_product">
                                        <p>{{ $item->nama_produk }}<br> <span class="rupiah">Rp.
                                                {{ $item->harga }}</span></p>
                                    </div>
                                </div>
                            </a></div>
                    @endforeach
                </div>
                <div class="p-4 text-center title-hero">
                    <a href="{{ url('/produk_tas') }}"><button class="btn-cstm-blue">Show More</button></a>
                </div>
            </div>
        </div>
    </div>
@endsection
