@extends('front.layout.default')

@section('content')
    {{-- <style>
    @media (max-width:768){
        .keranjang__navbar {
            position: fixed;
            top: 0;
            right: 0;
            width: 100%!important;
            background-color: #fff;
            padding: 10px;
            z-index: 97;
        }
    }
</style> --}}
    <div class="content">
        <div class="container__keranjang">
            <div class="bg-white tab-nav keranjang__navbar">
                <ul class="pt-3 nav nav-pills nav-justified">
                    <li class="nav-item m-3 {{ @$li_active == 'keranjang' ? 'active_' : '' }} d-flex align-items-center"
                        style="cursor:pointer;">
                        <a class="nav-link" data-id="1" id="tab1-tab">Keranjang</a>
                    </li>
                    <li class="m-3 nav-item d-flex align-items-center" style="cursor:pointer;">
                        <a class="nav-link" data-id="2" id="tab2-tab">Belum Pembayaran</a>
                    </li>
                    <li class="m-3 nav-item d-flex align-items-center" style="cursor:pointer;">
                        <a class="nav-link" data-id="3" id="tab3-tab">Produk Expired</a>
                    </li>
                    <li class="m-3 nav-item d-flex align-items-center" style="cursor:pointer;">
                        <a class="nav-link" data-id="4" id="tab4-tab">Sedang Proses</a>
                    </li>
                    <li class="m-3 nav-item d-flex align-items-center" style="cursor:pointer;">
                        <a class="nav-link" data-id="5" id="tab5-tab">Riwayat Pemesanan</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="p-4 row">
            <div class="col-8">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="tab-content" id="myTabContent">
            <div class="p-4 tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab"
                id="keranjang">
                @if (count(@$dt_keranjang) > 0)
                    @foreach (@$dt_keranjang as $item)
                        <div class="mt-2 shadow-sm card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-2">
                                        <img src="{{ asset('storage') . '/' . @$item->foto }}"
                                            style="width: 100%;object-fit:cover;">
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="title__produk__cart">
                                            <h3>{{ @$item->nama_produk }}</h3>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>Rp. {{ @$item->harga }},00</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>{{ @$item->jumlah }}</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <p>
                                                {{ @$item->deskripsi }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                        <div class="button__checkout">
                                            {{-- <button class="btn w-75 btn-cstm-green" data-id_cart="{{ @$item->id }}"
                                                onclick="checkout(this)">CheckOut</button> --}}
                                            <button class="btn w-75 btn-cstm-green"
                                                data-id_cart="{{ @$item->id }}">CheckOut</button>
                                            {{-- <button class="mt-2 btn w-75 btn-cstm-red" data-id_cart="{{ @$item->id }}"
                                                onclick="hapus(this)">Batal</button> --}}
                                            <button class="mt-2 btn w-75 btn-cstm-red"
                                                data-id_cart="{{ @$item->id }}">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="p-5 pagination d-flex justify-content-center" style="min-height: 70vh;">
                        <h3 class="text-center" style="opacity:80%;">Keranjang Anda Kosong.</h3>
                    </div>
                @endif
            </div>
            <div class="p-4 tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
                @if (count(@$dt_belum_bayar) > 0)
                    @foreach (@$dt_belum_bayar as $item)
                        <div class="mt-2 shadow-sm card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-2">
                                        <img src="{{ asset('storage') . '/' . @$item->foto }}"
                                            style="width: 100%;object-fit:cover;">
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="title__produk__cart">
                                            <h3>{{ @$item->nama_produk }}</h3>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>Rp. {{ @$item->harga }},00</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>{{ @$item->jumlah }}</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <p>
                                                {{ @$item->deskripsi }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                        <div class="button__checkout">
                                            @if (@$item->status_pembayaran == '0')
                                                <p class="badge bg-danger" style="margin-bottom:5px;">Belum Upload Bukti</p>
                                            @else
                                                <p class="badge bg-primary" style="margin-bottom:5px;">Menunggu Persetujuan
                                                </p>
                                            @endif
                                            <button class="mt-2 btn w-75 btn-cstm-blue"
                                                data-id_transaksi="{{ @$item->id }}"
                                                data-link="{{ asset('storage') . '/' . @$item->bukti_transfer }}"
                                                data-bukti="{{ @$item->bukti_transfer }}"
                                                data-nama_bank="{{ $item->nama_bank }}"
                                                data-rekening="{{ $item->rekening }}" onclick="uploadPay(this)">Upload
                                                Bukti Pembayaran</button>
                                            <div class="mt-2 tgl-exp">
                                                <p class="badge bg-info" style="margin-bottom:5px;">Terakhir Pembayaran
                                                    :<br> {{ tanggal_indonesia(@$item->tgl_exp) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="modal fade" id="uploadPay" tabindex="-1" aria-labelledby="uploadPayLabel"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="{{ url('upload_bukti') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="uploadPayLabel">Upload Bukti
                                            Pembayaran
                                        </h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <label>Pembayaran Melalui :</label>
                                        <p id="pembayaran"></p>
                                        <div id="bukti" style="display: none;">
                                            <a href="" target="_blank" class="btn btn-warning btn-sm"
                                                id="lihat_bukti">Lihat
                                                Bukti</a>
                                        </div>
                                        <div>
                                            <label>Upload File</label>
                                            <input type="file" name="upload_bukti" class="form-control"
                                                id="upload_bukti" value="" accept="image/*" capture>
                                            <input type="hidden" name="id_transaksi" id="id_transaksi">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Tutup</button>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @else
                    <div class="p-5 pagination d-flex justify-content-center" style="min-height: 70vh;">
                        <h3 class="text-center" style="opacity:80%;">Anda Belum Mempunyai Transaksi.</h3>
                    </div>
                @endif
            </div>
            <div class="p-4 tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
                @if (count(@$dt_exp) > 0)
                    @foreach (@$dt_exp as $item)
                        <div class="mt-2 shadow-sm card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-2">
                                        <img src="{{ asset('storage') . '/' . @$item->foto }}"
                                            style="width: 100%;object-fit:cover;">
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="title__produk__cart">
                                            <h3>{{ @$item->nama_produk }}</h3>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>Rp. {{ @$item->harga }},00</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>{{ @$item->jumlah }}</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <p>
                                                {{ @$item->deskripsi }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                        <div class="button__checkout">
                                            <div class="mt-2 tgl-exp">
                                                <p class="badge bg-info" style="margin-bottom:5px;">Terakhir Pembayaran
                                                    :<br> {{ tanggal_indonesia(@$item->tgl_exp) }}</p>
                                            </div>
                                            <button class="btn w-75 btn-cstm-green" data-id_cart="{{ @$item->id }}"
                                                onclick="checkout(this)">CheckOut Ulang</button>
                                            <button class="mt-2 btn w-75 btn-cstm-red" data-id_cart="{{ @$item->id }}"
                                                onclick="hapus(this)">Hapus</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="p-5 pagination d-flex justify-content-center" style="min-height: 70vh;">
                        <h3 class="text-center" style="opacity:80%;">Anda Belum Mempunyai Produk Expired.</h3>
                    </div>
                @endif
            </div>
            <div class="p-4 tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
                @if (count(@$dt_proses) > 0)
                    @foreach (@$dt_proses as $item)
                        <div class="mt-2 shadow-sm card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-2">
                                        <img src="{{ asset('storage') . '/' . @$item->foto }}"
                                            style="width: 100%;object-fit:cover;">
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="title__produk__cart">
                                            <h3>{{ @$item->nama_produk }}</h3>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>Rp. {{ @$item->harga }},00</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>{{ @$item->jumlah }}</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <p>
                                                {{ @$item->deskripsi }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                        <div class="button__checkout">
                                            @if (@$item->status_barang == '0')
                                                <p class="badge bg-danger" style="margin-bottom:5px;">Menunggu Persetujuan
                                                </p>
                                            @elseif(@$item->status_barang == '1')
                                                <p class="badge bg-warning" style="margin-bottom:5px;">Sedang Dikemas
                                                </p>
                                            @elseif(@$item->status_barang == '2')
                                                <div>
                                                    <p class="badge bg-primary" style="margin-bottom:5px;">Proses
                                                        Pengiriman
                                                    </p>
                                                </div>
                                                <div>
                                                    <button onclick="terima({{ @$item->id }})"
                                                        class="btn btn-sm btn-secondary">Pesanan diterima</button>
                                                </div>
                                            @endif
                                            <input type="hidden" id="link_bukti"
                                                value="{{ asset('storage') . '/' . @$item->bukti_transfer }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="p-5 pagination d-flex justify-content-center" style="min-height: 70vh;">
                        <h3 class="text-center" style="opacity:80%;">Belum ada Transaksi.</h3>
                    </div>
                @endif
            </div>
            <div class="p-4 tab-pane fade" id="tab5" role="tabpanel" aria-labelledby="tab5-tab">
                @if (count(@$dt_riwayat) > 0)
                    @foreach (@$dt_riwayat as $item)
                        <div class="mt-2 shadow-sm card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-2">
                                        <img src="{{ asset('storage') . '/' . @$item->foto }}"
                                            style="width: 100%;object-fit:cover;">
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="title__produk__cart">
                                            <h3>{{ @$item->nama_produk }}</h3>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>Rp. {{ @$item->harga }},00</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <h5>{{ @$item->jumlah }}</h5>
                                        </div>
                                        <div class="keterangan__produk">
                                            <p>
                                                {{ @$item->deskripsi }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 d-flex justify-content-center align-items-center">
                                        <div class="button__checkout">
                                            <a href="{{ url('/cetak/transaksi') . '/' . $item->id }}" target="_blank"
                                                class="mb-3 text-white shadow-sm d-none d-sm-inline-block btn btn-sm btn-warning"><i
                                                    class="text-white fas fa-print fa-sm fa-fw"></i> Cetak Invoice</a><br>
                                            <p class="badge bg-success" style="margin-bottom:5px;">Pesanan Diterima pada
                                                Tanggal <br> {{ tanggal_indonesia(@$item->tgl_terima) }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="p-5 pagination d-flex justify-content-center" style="min-height: 70vh;">
                        <h3 class="text-center" style="opacity:80%;">Anda Belum Mempunyai Riwayat Pemesanan.</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <script>
        $(".nav-pills .nav-item .nav-link").click(function() {
            id = $(this).data('id');
            $("#myTabContent .tab-pane").removeClass('show active')
            $(`#myTabContent #tab${id}`).addClass('show active');
        })
    </script>
@endsection
