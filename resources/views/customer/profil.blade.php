@extends('front.layout.default')

@section('content')
    <div class="content">
        <div class="row" style="padding: 50px;">
            <div class="hero__content d-flex justify-content-center align-items-center" style="min-height: 200px;">
                <div class="title-hero">
                    <h1 class="text-center fw-bold" style="vertical-align: middle;">Profil User</h1>
                </div>
            </div>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('simpan_profil') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="mb-3">
                            <label for="username" class="form-label">Username</label>
                            <input type="text" class="form-control" name="username" id="username"
                                value="{{ Auth::user()->username }}" placeholder="Username">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama" id="nama"
                                value="{{ Auth::user()->name }}" placeholder="Nama Lengkap">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="text" class="form-control" name="email" id="email"
                                value="{{ Auth::user()->email }}" placeholder="Masukkan Email">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="mb-3">
                            <label for="no_hp" class="form-label">Nomor HP</label>
                            <input type="text" class="form-control" name="no_hp" id="no_hp"
                                value="{{ Auth::user()->no_hp }}" placeholder="Masukkan Nomor HP">
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        @if (!empty(@$alamat))
                            <input type="hidden" name="id_alamat" value="{{ $alamat->id }}">
                        @endif
                        <div class="mb-3">
                            <label for="provinsi" class="form-label">Provinsi</label>
                            <select name="provinsi" id="provinsi" class="form-control">
                                <option value="">-- Pilih Provinsi --</option>
                                @foreach ($ref_provinsi as $v)
                                    <option value="{{ $v->province_id }}"
                                        {{ @$alamat->province_id == $v->province_id ? 'selected' : '' }}>
                                        {{ $v->province_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="mb-3">
                            <label for="kabupaten" class="form-label">Kabupaten</label>
                            <select name="kabupaten" id="kabupaten" class="form-control">
                                @if (!empty(@$alamat->city_id))
                                    <option value="{{ $alamat->city_id }}">{{ $alamat->kabupaten }}</option>
                                @else
                                    <option value="">-- Pilih Provinsi Dahulu --</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="mb-3">
                            <label for="kecamatan" class="form-label">Kecamatan</label>
                            <select name="kecamatan" id="kecamatan" class="form-control">
                                @if (!empty(@$alamat->subdistrict_id))
                                    <option value="{{ $alamat->subdistrict_id }}">{{ $alamat->kecamatan }}</option>
                                @else
                                    <option value="">-- Pilih Kecamatan Dahulu --</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-12">
                        <div class="mb-3">
                            <label for="alamat" class="form-label">Alamat</label>
                            <textarea name="alamat" id="alamat" class="form-control" rows="5">{{ @$alamat->detail_alamat }}</textarea>
                        </div>
                    </div>
                    <button type="submit" class="mt-4 btn-cstm-blue">Simpan</button>
                </div>
            </form>
        </div>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    </div>
    <script>
        $(document).ready(function() {
            $("#provinsi").select2();
            $("#kabupaten").select2();
            $("#kecamatan").select2();
        })
    </script>
@endsection
