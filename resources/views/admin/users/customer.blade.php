@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3">{{ @$page_title }}</h1>
            {{-- <button type="button" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary" data-toggle="modal"
                data-target="#exampleModal">
                Tambah Admin
            </button> --}}
        </div>
        {{-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Admin</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="{{ url('admin/tambah_admin') }}" method="post">
                        @csrf
                        <div class="modal-body">
                            <div class="p-3 row">
                                <div class="mt-3 col-12 col-md-6">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="username">
                                </div>
                                <div class="mt-3 col-12 col-md-6">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="nama">
                                </div>
                                <div class="mt-3 col-12 col-md-6">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email">
                                </div>
                                <div class="mt-3 col-12 col-md-6">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" value="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div> --}}

        <!-- Content Row -->
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif

                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="table-responsive">
                                <table class="table table-fixed table-bordered" id="dataTable" width="100%"
                                    cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Username</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($dt_user as $row)
                                            <tr>
                                                <td style="width:5%!important;">{{ $no++ }}</td>
                                                <td style="width:20%!important;">{{ $row->username }}</td>
                                                <td style="width:20%!important;">
                                                    {{ $row->name }}<br>
                                                    <b>Email : </b>{{ $row->email }}<br>
                                                    <b>Nomor HP : </b>{{ $row->no_hp }}
                                                </td>
                                                <td style="width:30%!important;">
                                                    {{ $row->detail_alamat }}
                                                </td>
                                                <td class="text-center" style="width:10%!important;">
                                                    <button class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger"
                                                        data-id="{{ $row->id }}" onclick="hapus(this)"><i
                                                            class="text-white fas fa-trash fa-sm fa-fw"></i></button>
                                                    <a
                                                        href="{{ url('/edit/produk') . '/' . $row->id }}"class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-warning"><i
                                                            class="text-white fas fa-edit fa-sm fa-fw"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
@endsection
