@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3">{{ @$page_title }}</h1>
            {{-- <a href="{{ url('admin/tambah/produk') }}" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">
                <i class="text-white fas fa-plus fa-sm fa-fw"></i> Tambah Produk</a> --}}
        </div>

        <!-- Content Row -->
        <div class="card">
            <div class="card-body">
                <form action="{{ route('pemasukan_filter') }}" method="get">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="tanggal_mulai">Tanggal Mulai</label>
                                <input type="date" class="form-control" name="tanggal_mulai" value="{{ @$filter['mulai'] }}">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="tanggal_selesai">Tanggal Selesai</label>
                                <input type="date" class="form-control" name="tanggal_selesai" value="{{ @$filter['selesai'] }}">
                            </div>
                        </div>
                        <div class="col-12 col-md-4 align-items-end d-flex">
                            <div class="form-group me-3">
                                <button type="submit" class="btn btn-success">Terapkan</button>
                            </div>
                            <div class="form-group">
                                <a href="{{ route('pemasukan') }}" class="btn btn-outline-primary">Reset</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="mt-3 card" style="width: 100%;">
            <div class="card-body">
                <div class="col-12">
                    <div class="row">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        <div class="table-responsive">
                            <a href="{{ url('admin/cetak/cetak_transaksi?tgl_mulai=' . urlencode(@$filter['mulai']) . '&tgl_selesai=' . urlencode(@$filter['selesai'])) }}" target="_blank" class="btn btn-sm btn-primary">Cetak Rekap</a>

                            {{-- <a href="{{ url('admin/cetak/cetak_transaksi?tgl_mulai={{ @$filter['mulai'] }}&tgl_selesai={{ @$filter['selesai'] }}') }}" target="_blank" class="btn btn-sm btn-primary">Cetak Rekap</a> --}}
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Nama Customer</th>
                                        <th>Detail</th>
                                        <th>Total</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($pemasukan as $row)
                                        <tr>
                                            <td style="width:5%!important;">{{ $no++ }}</td>
                                            <td style="width:20%!important;">{{ tanggal_indonesia($row->tgl_transaksi) }}
                                            </td>
                                            <td style="width:25%!important;">{{ @$row->name }}</td>
                                            <td style="width:10%!important;">
                                                <button type="button" class="btn btn-sm btn-primary"
                                                    onclick="show_detail('{{ $row->id }}')">
                                                    Lihat Detail
                                                </button>
                                            </td>
                                            <td style="width:20%!important;">Rp. {{ rupiah(@$row->harga_total) }}
                                            </td>
                                            <td class="text-center" style="width:10%!important;">
                                                {{-- <button class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger"
                                                    data-id="{{ $row->id }}" onclick="hapus(this)"><i
                                                        class="text-white fas fa-trash fa-sm fa-fw"></i></button> --}}
                                                <a
                                                    href="{{ url('/cetak/transaksi') . '/' . $row->id }}" target="_blank" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-warning"><i
                                                        class="text-white fas fa-print fa-sm fa-fw"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-content">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
