<a href="{{ url('admin/cetak/transaksi_bulan/' . $bulan) }}" target="_blank" class="btn btn-sm btn-primary">Cetak Rekap</a>
<table class="table table-bordered" id="table-transaksi">
    <thead>
        <th>No</th>
        <th>Nama</th>
        <th>Total</th>
    </thead>
    <tbody>
        @php
            $total = 0;
        @endphp
        @foreach ($dt_pemasukan as $item)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $item->name }}</td>
                <td>Rp {{ rupiah($item->harga_total) }}</td>
            </tr>
            @php
                $total += $item->harga_total;
            @endphp
        @endforeach
    </tbody>
    <tfoot>
        <th colspan="2">Total</th>
        <th>Rp {{ rupiah(@$total) }}</th>
    </tfoot>
</table>

<script>
    $(document).ready(function() {
        $("#table-transaksi").DataTable({
            responsive: true
        });
    })
</script>
