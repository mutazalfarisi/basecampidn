<div class="p-4">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Gambar</th>
                <th>Ukuran</th>
                <th>Jumlah</th>
                <th>Ongkir</th>
                <th>Harga Total</th>
            </tr>
        </thead>
        <tbody>
            @php
                $no = 1;
            @endphp

            @foreach ($data as $item)
                <tr>
                    <td style="width: 5%;">{{ $no++ }}</td>
                    <td style="width: 20%;">{{ $item->nama_produk }}</td>
                    <td style="width: 20%;text-align:center;"><img src="{{ asset('storage/' . $item->foto) }}"style="width:70%;"></td>
                    <td style="width: 5%;">{{ $item->ukuran }}</td>
                    <td style="width: 5%;">{{ $item->jml_barang }}</td>
                    <td style="width: 20%;">Rp {{ rupiah($item->harga_ongkir) }}</td>
                    <td style="width: 20%;">Rp {{ rupiah($item->harga_total) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
