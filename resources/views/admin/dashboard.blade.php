@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3">{{ @$page_title }}</h1>
            <a href="{{ url('/admin/alamat_toko') }}" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">
                <i class="text-gray-400 fas fa-user fa-sm fa-fw"></i> Alamat Toko</a>
        </div>

        <!-- Content Row -->
        <div class="row">
            {{-- Pembayaran Lunas --}}
            <div class="mb-4 col-xl-3 col-md-6">
                <div class="py-2 shadow card border-left-primary h-100">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="mr-2 col">
                                <div class="mb-1 text-xs font-weight-bold text-primary text-uppercase">
                                    Transaksi Bulan Ini</div>
                                <div class="mb-0 text-gray-800 h5 font-weight-bold">{{ $transaksi_bulan_ini }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="text-gray-300 fas fa-calendar fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="mb-4 col-xl-3 col-md-6">
                <div class="py-2 shadow card border-left-success h-100">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="mr-2 col">
                                <div class="mb-1 text-xs font-weight-bold text-success text-uppercase">
                                    Transaksi Menunggu Validasi
                                </div>
                                <div class="mb-0 text-gray-800 h5 font-weight-bold">{{ $menunggu_validasi }}</div>

                            </div>
                            <div class="col-auto">
                                <i class="text-gray-300 fas fa-dollar-sign fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="mb-4 col-xl-3 col-md-6">
                <div class="py-2 shadow card border-left-info h-100">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="mr-2 col">
                                <div class="mb-1 text-xs font-weight-bold text-info text-uppercase">
                                    Pemasukan Bulan @php
                                        echo getBulan();
                                    @endphp
                                </div>
                                <div class="row no-gutters align-items-center">
                                    <div class="col-auto">
                                        <div class="mb-0 mr-3 text-gray-800 h5 font-weight-bold">Rp {{ rupiah($pemasukan) }}
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mr-2 progress progress-sm">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%"
                                                aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="text-gray-300 fas fa-clipboard-list fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="mb-4 col-xl-3 col-md-6">
                <div class="py-2 shadow card border-left-warning h-100">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="mr-2 col">
                                <div class="mb-1 text-xs font-weight-bold text-warning text-uppercase">
                                    Produk Perlu Di Restok</div>
                                <div class="mb-0 text-gray-800 h5 font-weight-bold">{{ $restok }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="text-gray-300 fas fa-store-slash fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Content Row -->

        {{-- <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
                <div class="mb-4 shadow card">
                    <!-- Card Header - Dropdown -->
                    <div class="flex-row py-3 card-header d-flex align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Transaksi Bulan <span
                                id="nama_bulan">@php echo getBulan(); @endphp</span></h6>
                        <select id="pilih_bulan" class="m-0 font-weight-bold text-primary form-select-sm example">
                            <option disabled>Pilih Bulan</option>
                            <option value="01"{{ $pilih_bulan == '01' ? 'selected' : '' }}>Januari</option>
                            <option value="02"{{ $pilih_bulan == '02' ? 'selected' : '' }}>Februari</option>
                            <option value="03" {{ $pilih_bulan == '03' ? 'selected' : '' }}>Maret</option>
                            <option value="04"{{ $pilih_bulan == '04' ? 'selected' : '' }}>April</option>
                            <option value="05"{{ $pilih_bulan == '05' ? 'selected' : '' }}>Mei</option>
                            <option value="06"{{ $pilih_bulan == '06' ? 'selected' : '' }}>Juni</option>
                            <option value="07"{{ $pilih_bulan == '07' ? 'selected' : '' }}>Juli</option>
                            <option value="08"{{ $pilih_bulan == '08' ? 'selected' : '' }}>Agustus</option>
                            <option value="09"{{ $pilih_bulan == '09' ? 'selected' : '' }}>September</option>
                            <option value="10"{{ $pilih_bulan == '10' ? 'selected' : '' }}>Oktober</option>
                            <option value="11"{{ $pilih_bulan == '11' ? 'selected' : '' }}>November</option>
                            <option value="12"{{ $pilih_bulan == '12' ? 'selected' : '' }}>Desember</option>
                        </select>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div id="rekap_bulan">
                            <a href="{{ url('admin/cetak/transaksi_bulan/' . date('m')) }}" target="_blank"
                                class="btn btn-sm btn-primary">Cetak Rekap</a>
                            <table class="table table-bordered" id="table-transaksi">
                                <thead>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Total</th>
                                </thead>
                                <tbody>
                                    @php
                                        $total = 0;
                                    @endphp
                                    @foreach ($dt_pemasukan as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>Rp {{ rupiah($item->harga_total) }}</td>
                                        </tr>
                                        @php
                                            $total += $item->harga_total;
                                        @endphp
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <th colspan="2">Total</th>
                                    <th>Rp {{ rupiah(@$total) }}</th>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
                <div class="mb-4 shadow card">
                    <!-- Card Header - Dropdown -->
                    <div class="flex-row py-3 card-header d-flex align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Produk Perlu Restok</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <table class="table table-bordered" id="table-restok">
                            <thead>
                                <th>No</th>
                                <th>Nama Barang</th>
                            </thead>
                            <tbody>
                                @foreach ($dt_restok as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->nama_produk }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- Content Row -->
        <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
                <div class="mb-4 shadow card">
                    <!-- Card Header - Dropdown -->
                    <div class="flex-row py-3 card-header d-flex align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Rekap Pemasukan Tahun @php
                            echo date('Y');
                        @endphp</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div id="chartdiv"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- End of Main Content -->
    <style>
        #chartdiv {
            width: 100%;
            height: 500px;
        }
    </style>

    <!-- Resources -->
    <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>

    <!-- Chart code -->
    <script>
        am5.ready(function() {
            // Create root element
            // https://www.amcharts.com/docs/v5/getting-started/#Root_element
            var root = am5.Root.new("chartdiv");

            // Set themes
            // https://www.amcharts.com/docs/v5/concepts/themes/
            root.setThemes([
                am5themes_Animated.new(root)
            ]);

            // Create chart
            // https://www.amcharts.com/docs/v5/charts/xy-chart/
            var chart = root.container.children.push(am5xy.XYChart.new(root, {
                panX: true,
                panY: true,
                wheelX: "panX",
                wheelY: "zoomX",
                pinchZoomX: true,
                paddingLeft: 0,
                paddingRight: 1
            }));

            // Add cursor
            // https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
            var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
            cursor.lineY.set("visible", false);


            // Create axes
            // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
            var xRenderer = am5xy.AxisRendererX.new(root, {
                minGridDistance: 30,
                minorGridEnabled: true
            });

            xRenderer.labels.template.setAll({
                rotation: -90,
                centerY: am5.p50,
                centerX: am5.p100,
                paddingRight: 15
            });

            xRenderer.grid.template.setAll({
                location: 1
            })

            var xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
                maxDeviation: 0.3,
                categoryField: "bulan",
                renderer: xRenderer,
                tooltip: am5.Tooltip.new(root, {})
            }));

            var yRenderer = am5xy.AxisRendererY.new(root, {
                strokeOpacity: 0.1
            })

            var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
                maxDeviation: 0.3,
                renderer: yRenderer
            }));

            // Create series
            // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
            var series = chart.series.push(am5xy.ColumnSeries.new(root, {
                name: "Series 1",
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: "harga",
                sequencedInterpolation: true,
                categoryXField: "bulan",
                tooltip: am5.Tooltip.new(root, {
                    labelText: "{valueY}"
                })
            }));

            series.columns.template.setAll({
                cornerRadiusTL: 5,
                cornerRadiusTR: 5,
                strokeOpacity: 0
            });
            series.columns.template.adapters.add("fill", function(fill, target) {
                return chart.get("colors").getIndex(series.columns.indexOf(target));
            });

            series.columns.template.adapters.add("stroke", function(stroke, target) {
                return chart.get("colors").getIndex(series.columns.indexOf(target));
            });

            var dt = '<?php echo json_encode($dt_rekap); ?>';
            // Set data
            var data = JSON.parse(dt);
            xAxis.data.setAll(data);
            series.data.setAll(data);


            // Make stuff animate on load
            // https://www.amcharts.com/docs/v5/concepts/animations/
            series.appear(1000);
            chart.appear(1000, 100);

        }); // end am5.ready()
    </script>

    <script>
        $(document).ready(function() {
            $("#table-transaksi").DataTable({
                responsive: true
            });
            $("#table-restok").DataTable({
                responsive: true
            });

            $("#pilih_bulan").change(function() {
                $("#rekap_bulan").html('<div class="text-center">Loading..</div>')
                var bulan = $(this).find('option:selected').text();
                $("#nama_bulan").html(bulan);
                $.ajax({
                    url: '/admin/get_rekap_bulan',
                    type: 'get',
                    data: {
                        bulan: $("#pilih_bulan").val()
                    },
                    dataType: 'json',
                    success: function(res) {
                        $("#rekap_bulan").html(res.view);
                    }
                })
                console.log($("#pilih_bulan").val());

            })
        })
    </script>
@endsection
