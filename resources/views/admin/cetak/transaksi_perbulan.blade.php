<!-- resources/views/pdf/example.blade.php -->

<!DOCTYPE html>
<html>

<head>
    <title>Pemasukan Bulan <?= getBulanke($bulan) ?></title>
</head>
<style>
    table {
        width: 100%;
        border-collapse: collapse;
    }

    table,
    th,
    td {
        border: 1px solid black;
    }

    th,
    td {
        padding: 8px;
        text-align: left;
    }
</style>

<body>
    <div style="text-align: center;">
        <header>
            <h2 style="margin-bottom: 0px;">{{ @$store->nama_toko }}</h2>
            <p style="font-size:10px;">{{ ucwords(strtolower($store->alamat_toko)) }},
                {{ $store->kabupaten . ' Kecamatan ' . $store->kecamatan . ' Provinsi ' . $store->provinsi . ', ' . $store->no_hp }}
            </p>
        </header>
        <hr>
        <div class="content" style="margin-top: 30px;">
            <h4 style="text-align:center;">Transaksi Bulan <?= getBulanke($bulan) ?></h4>
            <table class="">
                <thead>
                    <th>No</th>
                    <th>Nama Customer</th>
                    <th>Total</th>
                </thead>
                <tbody>
                    @php
                        $total = 0;
                    @endphp
                    @foreach ($data as $item)
                        <tr>
                            <td style="width: 5%;">{{ $loop->iteration }}</td>
                            <td style="width: 50%;">{{ @$item->name }}</td>
                            <td style="width: 45%;">Rp {{ rupiah(@$item->harga_total) }}</td>
                        </tr>
                        @php
                            $total += $item->harga_total;
                        @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <th colspan="2">Total</th>
                    <th>Rp {{ rupiah($total) }}</th>
                </tfoot>
            </table>
        </div>
    </div>
</body>

</html>
