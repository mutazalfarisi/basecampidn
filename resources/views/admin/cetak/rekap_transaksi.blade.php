<!-- resources/views/pdf/example.blade.php -->

<!DOCTYPE html>
<html>

<head>
    <title>Rekap Traksaksi</title>
</head>
<style>
    table{
        width: 100%;
        border-collapse: collapse;
    }

    table, th, td{
        border: 1px solid black;
    }

    th, td{
        padding: 8px;
        text-align: left;
    }
</style>
<body>
    <div style="text-align: center;">
        <header>
            <h2 style="margin-bottom: 0px;">{{ @$store->nama_toko }}</h2>
            <p style="font-size:10px;">{{ ucwords(strtolower($store->alamat_toko)) }},
                {{ $store->kabupaten . ' Kecamatan ' . $store->kecamatan . ' Provinsi ' . $store->provinsi . ', ' . $store->no_hp }}
            </p>
        </header>
        <hr>
        <div class="content" style="margin-top: 30px;">
            <h4 style="text-align:center;">Rekap Pemasukan</h4>
            <table class="">
                <thead>
                    <th>No</th>
                    <th>Tanggal Transaksi</th>
                    <th>Nama Customer</th>
                    <th>Total</th>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td style="width: 5px;">{{ $loop->iteration }}</td>
                            <td style="width: 30px;">{{ tanggal_indonesia($item->tgl_transaksi) }}</td>
                            <td style="width: 50px;">{{ @$item->name }}</td>
                            <td style="width: 25px;">Rp {{ rupiah(@$item->harga_total) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
