</div>
<!-- Footer -->
<footer class="bg-white sticky-footer">
    <div class="container my-auto">
        <div class="my-auto text-center copyright">
            <span>Copyright &copy; Basecampidn 2023</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="rounded scroll-to-top" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Anda akan keluar</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Apakah anda yakin ingin logout?</div>
            <form id="logout-form" action="{{ route('logout_admin') }}" method="POST" style="display: none;">
                @csrf
            </form>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" href="{{ route('logout_admin') }}">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="{{ asset('assets_tambah') }}/vendor/jquery/jquery.min.js"></script>
<script src="{{ asset('assets_tambah') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="{{ asset('assets_tambah') }}/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="{{ asset('assets_tambah') }}/js/sb-admin-2.min.js"></script>

<script src="{{ asset('assets_tambah') }}/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('assets_tambah') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Page level plugins -->
<script src="{{ asset('assets_tambah') }}/vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="{{ asset('assets_tambah') }}/js/demo/chart-area-demo.js"></script>
<script src="{{ asset('assets_tambah') }}/js/demo/chart-pie-demo.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@if (!empty(@$script_js))
    <script src="{{ asset(@$script_js) }}"></script>
@endif



</body>

</html>
