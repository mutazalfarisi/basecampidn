<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('admin/dashboard') }}">
                <div class="mx-3 sidebar-brand-text">Basecampidn</sup></div>
            </a>

            <!-- Divider -->
            <hr class="my-0 sidebar-divider">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item {{ @$li_active == 'dashboard' ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/admin/dashboard') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <hr class="my-0 sidebar-divider">
            <!-- Divider -->

            {{-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item {{ @$li_active == 'manajemen_user' ? 'active' : '' }}">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#manajemen_user"
                    aria-expanded="true" aria-controls="manajemen_user">
                    <i class="fas fa-fw fa-user"></i>
                    <span>Manajemen User</span>
                </a>
                <div id="manajemen_user" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="py-2 bg-white rounded collapse-inner">
                        <a class="collapse-item {{ @$li_sub_active == 'daftar_admin' ? 'active' : '' }}"
                            href="{{ url('/admin/daftar_admin') }}">Daftar Admin</a>
                        <a class="collapse-item {{ @$li_sub_active == 'daftar_customer' ? 'active' : '' }}"
                            href="{{ url('/admin/daftar_customer') }}">Daftar Customer</a>
                    </div>
                </div>
            </li> --}}

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Kelola Website
            </div>

            {{-- Nav Item - Pages Collapse Menu --> --}}
            <li class="nav-item {{ @$li_active == 'produk' ? 'active' : '' }}">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#produk"
                    aria-expanded="true" aria-controls="produk">
                    <i class="fas fa-fw fa-tshirt"></i>
                    <span>Kelola Produk</span>
                </a>
                <div id="produk" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="py-2 bg-white rounded collapse-inner">
                        <a class="collapse-item {{ @$li_sub_active == 'kelola_kaos' ? 'active' : '' }}"
                            href="{{ url('/admin/kelola/kaos') }}">Produk Kaos</a>
                        <a class="collapse-item {{ @$li_sub_active == 'kelola_tas' ? 'active' : '' }}"
                            href="{{ url('/admin/kelola/tas') }}">Produk Tas</a>
                    </div>
                </div>
            </li>

            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Kelola Transaksi
            </div>

            <li class="nav-item {{ @$li_active == 'transaksi' ? 'active' : '' }}">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#transaksi"
                    aria-expanded="true" aria-controls="transaksi">
                    <i class="fas fa-fw fa-tasks"></i>
                    <span>Kelola Transaksi</span>
                </a>
                <div id="transaksi" class="collapse" aria-labelledby="transaksi" data-parent="#accordionSidebar">
                    <div class="py-2 bg-white rounded collapse-inner">
                        <a class="collapse-item {{ @$li_sub_active == 'transaksi_kaos' ? 'active' : '' }}"
                            {{-- href="{{ url('/admin/transaksi/kaos') }}">Transaksi Kaos</a> --}} href="#">Transaksi Kaos</a>
                        <a class="collapse-item {{ @$li_sub_active == 'transaksi_tas' ? 'active' : '' }}"
                            href="{{ url('/admin/transaksi/tas') }}">Transaksi Tas</a>
                    </div>
                </div>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <div class="sidebar-heading">
                Kelola Keuangan
            </div>

            <li class="nav-item {{ @$li_active == 'pemasukan' ? 'active' : '' }}">
                {{-- <a class="nav-link" href="{{ url('/admin/pemasukan') }}"> --}}
                <a class="nav-link" href="#">
                    <i class="fas fa-fw fa-wallet"></i>
                    <span>Pemasukan</span></a>
            </li>

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="border-0 rounded-circle" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->
